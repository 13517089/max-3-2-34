import random

def max3(x,y,z):
    return random.choice([x,y,z])

x, y, z = map(int, input().split())

print(max3(x,y,z))

